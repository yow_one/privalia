package yow.one.feina;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.TextView;

import yow.one.feina.Objects.OMovies;
import yow.one.feina.Tasks.TaskDownloadImage;

/**
 * Created by Yow_one on 24/01/2018.
 */

public class ActivityDetails extends Activity {
    TextView title, year, overview;
    ImageView imageMovie;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.movie_details);
        Intent intent = getIntent();
        initComponents();
        OMovies movie = (OMovies) intent.getSerializableExtra(getString(R.string.idMovie));
        title.setText(movie.getTitle());
        year.setText(movie.getYear());
        overview.setText(movie.getOverview());
        TaskDownloadImage taskDownloadImage = new TaskDownloadImage();
        taskDownloadImage.setVariables(imageMovie, movie.getUrlImage());
        taskDownloadImage.execute();
    }

    public void initComponents(){
        title = findViewById(R.id.txtTitle2);
        year = findViewById(R.id.txtYear2);
        overview = findViewById(R.id.txtOverview2);
        imageMovie = findViewById(R.id.imageMovie2);
    }
}
