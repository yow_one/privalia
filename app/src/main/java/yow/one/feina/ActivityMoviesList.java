package yow.one.feina;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import yow.one.feina.Adapters.AdapterMovies;
import yow.one.feina.Interface.ILoadMore;
import yow.one.feina.Listeners.RecyclerItemClickListener;
import yow.one.feina.Objects.OMovies;
import yow.one.feina.Tasks.TaskGetList;
import yow.one.feina.Tasks.TaskGetMoreData;

import static android.nfc.tech.MifareUltralight.PAGE_SIZE;

/**
 * Created by Yow_one on 24/01/2018.
 */

public class ActivityMoviesList extends Activity {
    boolean isLoading;
    int visibleTreshold=5;
    int lastVisibleItem, totalItemCount;
    private RecyclerView mListView;
    private List<OMovies> mMovies = new ArrayList<>();
    private List<String> mMoviesId = new ArrayList<>();
    private List<String> mMoviesNames = new ArrayList<>();
    private AdapterMovies adapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private String token;
    private int page,count=1;
    ArrayAdapter<String> adapterAutoComplete;
    ActivityMoviesList activity;
    AutoCompleteTextView autoCompleteTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.list_movies);
        SharedPreferences preferences = this.getSharedPreferences(getString(R.string.requestToken), Context.MODE_PRIVATE);
        token =preferences.getString(getString(R.string.requestToken), "");
        activity = this;
        iniComponents();
    }

    private void iniComponents(){
        page =1;
        mListView = findViewById(R.id.listMoviesovies);
        mListView.setHasFixedSize(true);
        adapterAutoComplete = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, mMoviesNames);
        autoCompleteTextView = findViewById(R.id.autoCompleteTextView);
        autoCompleteTextView.setAdapter(adapterAutoComplete);
        autoCompleteTextView.setThreshold(1);
        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent inteent = new Intent(ActivityMoviesList.this, ActivityDetails.class);
                inteent.putExtra(getString(R.string.idMovie), mMovies.get(i));
                startActivity(inteent);
            }
        });
        mLayoutManager = new LinearLayoutManager(this);
        mListView.setLayoutManager(mLayoutManager);
        final LinearLayoutManager  layoutManager = (LinearLayoutManager) mListView.getLayoutManager();
        adapter = new AdapterMovies
                (this, mMovies, mListView);
        adapter.setLoadMore(new ILoadMore() {
            @Override
            public void onLoadMore() {
                if (mMovies.size() <= mMoviesId.size()){
                    mMovies.add(null);
                    adapter.notifyItemInserted(mMovies.size()-1);
                    Toast.makeText(ActivityMoviesList.this, "Load not completed", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mMovies.remove(mMovies.size()-1);
                            adapter.notifyItemRemoved(mMovies.size()-1);
                            int index = mMovies.size();
                            int end = index+10;
                            for (int i = index +1; i <= end; i++){
                                OMovies movie = new OMovies();
                                movie.setTitle("title "+ String.valueOf(i));
                                movie.setYear("");
                                movie.setOverview("adsfas");
                                mMovies.add(movie);
                                adapter.notifyItemInserted(mMovies.size());
                                adapter.setLoaded();
                            }
                        }
                    },5000);
                }else {
                    Toast.makeText(ActivityMoviesList.this, "Load completed", Toast.LENGTH_SHORT).show();
                }
            }
        });
        mListView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View v, int position) {
                        Intent i = new Intent( ActivityMoviesList.this, ActivityDetails.class);
                        i.putExtra(getString(R.string.idMovie), mMovies.get(position));
                        startActivity(i);
                    }
                })
        );
        mListView.setAdapter(adapter);
        TaskGetList taskGetList = new TaskGetList();
        taskGetList.setVariables(this, page, mMovies, adapter, mMoviesId, this);
        taskGetList.execute();
    }
    public void addLast(){
        adapter.setLoadMore(new ILoadMore() {
            @Override
            public void onLoadMore() {
                if (mMovies.size() <= mMoviesId.size()){
                    mMovies.add(null);
                    adapter.notifyItemInserted(mMovies.size()-1);
                    Toast.makeText(ActivityMoviesList.this, "Loading data", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mMovies.remove(mMovies.size()-1);
                            adapter.notifyItemRemoved(mMovies.size()-1);
                            int index = mMovies.size();
                            int end = index+6;
                            loadMoreData();
                        }
                    },5000);
                }else {
                    Toast.makeText(ActivityMoviesList.this, "Load completed", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    public void loadMoreData(){
        if (count < mMoviesId.size()){
            count = count+1;
            TaskGetMoreData taskGetMoreData = new TaskGetMoreData();
            taskGetMoreData.setVariables(this, adapter, count, mMovies, mMoviesId, activity);
            taskGetMoreData.execute();
        }
    }

    public void res(){



//        HttpClient client = new DefaultHttpClient();
//        List<NameValuePair> noms = new ArrayList<NameValuePair>();
//        noms.add(new BasicNameValuePair("authorization", "Bearer <"+ context.getString(R.string.ApiKey)+">"));
//        noms.add(new BasicNameValuePair("content-type", "application/json;charset=utf-8"));
//        ContentValues values=new ContentValues();
////        HttpPost enviarpost = new HttpPost("https://api.themoviedb.org/4/auth/request_token");
//        HttpGet enviarGet = new HttpGet("https://api.themoviedb.org/4/list/1?api_key=93aea0c77bc168d8bbce3918cefefa45");
//        StringBuilder sb = new StringBuilder();
//        int arrayJasona =0;
//        try{
////            enviarpost.setEntity(new UrlEncodedFormEntity(noms));
//            HttpResponse response = client.execute(enviarGet);
//            HttpEntity contingut = response.getEntity();
//            is=contingut.getContent();
//
//            BufferedReader buferlector = new BufferedReader(new InputStreamReader(is));
//            String linea = null;
//            while ((linea = buferlector.readLine())!=null){
//                sb.append(linea);
//            }
//            resultat = sb.toString();
//
//            JSONObject jsonObjectIds = new JSONObject(resultat);
//            JSONObject movies = jsonObjectIds.getJSONObject("object_ids");
//            JSONArray jarray = new JSONArray("[" + movies.toString()+ "]");
//            String[] pass1 = movies.toString().split(",");
//            List<String> moviesId = new ArrayList<>();
//            for(int i=0;i<pass1.length;i++) {
//                String[] pass2 = pass1[i].split(":");
//                moviesId.add(pass2[1].replace("\"",""));
//            }
//
//            Log.d("movies.toString()", movies.toString());
//
//            client = new DefaultHttpClient();
//            noms = new ArrayList<NameValuePair>();
//            noms.add(new BasicNameValuePair("authorization", "Bearer " + token ));
//            noms.add(new BasicNameValuePair("content-type", "application/json;charset=utf-8"));
////        HttpPost enviarpost = new HttpPost("https://api.themoviedb.org/4/auth/request_token");
//            enviarGet = new HttpGet("https://api.themoviedb.org/4/list/" + moviesId.get(0) + "?api_key=" + context.getString(R.string.ApiKey));
//            sb = new StringBuilder();
//
////            enviarpost.setEntity(new UrlEncodedFormEntity(noms));
//            response = client.execute(enviarGet);
//            contingut = response.getEntity();
//            is=contingut.getContent();
//
//            buferlector = new BufferedReader(new InputStreamReader(is));
//            linea = null;
//            while ((linea = buferlector.readLine())!=null){
//                sb.append(linea);
//            }
//            resultat = sb.toString();

//            Log.d("movies.toString()", movies.toString());






//            for(int i=0;i<movies.length();i++) {
//                JSONObject obj = movies.getJSONObject(i);
//                    Log.d("a", "1");
//                }
//            if (!resultat.equals("null")){
//                JSONObject jsonObject = new JSONObject(resultat);
//                token = jsonObject.getString(context.getString(R.string.requestToken));
//                success = jsonObject.getBoolean(context.getString(R.string.success));
//                tokenExpires = jsonObject.getString(context.getString(R.string.expires_at));
//                Log.d("token", token);
//                Log.d("success", String.valueOf(success));
//                Log.d("expires at", tokenExpires);
////                JSONArray arrayJson = new JSONArray(resultat);
////                int lengjason=arrayJson.length();
////                for(int i=0;i<arrayJson.length();i++){
////                    JSONObject objecteJson = arrayJson.getJSONObject(i);
////                    String token = objecteJson.getString(context.getString(R.string.requestToken));
////                    boolean success = objecteJson.getBoolean(context.getString(R.string.success));
////                    String tokenExpires = objecteJson.getString(context.getString(R.string.expires_at));
////                    Log.d("token", token);
////                    Log.d("success", String.valueOf(success));
////                    Log.d("expires at", tokenExpires);
////                }
//            }
//            OkHttpClient client1 = new OkHttpClient();
//
//            MediaType mediaType = MediaType.parse("application/octet-stream");
//            RequestBody body = RequestBody.create(mediaType, "{}");
//            Request request = new Request.Builder()
//                    .url("https://api.themoviedb.org/3/account/" + token + "/lists?page=1")
//                    .get()
//                    .addHeader("authorization", "Bearer <" + token + ">")
//                    .build();
//
//            Response response1 = client1.newCall(request).execute();
//            Log.d("response" ,response1.body().string());
////            client = new DefaultHttpClient();
////            noms = new ArrayList<NameValuePair>();
////            noms.add(new BasicNameValuePair("authorization", "Bearer <"+ token +">"));
////            values=new ContentValues();
//////        HttpPost enviarpost = new HttpPost("https://api.themoviedb.org/4/auth/request_token");
////            enviarGet = new HttpGet("https://api.themoviedb.org/3/account/%7Baccount_id%7D/lists?page=1");
////            sb = new StringBuilder();
////
//////            enviarpost.setEntity(new UrlEncodedFormEntity(noms));
////            response = client.execute(enviarGet);
////            contingut = response.getEntity();
////            is=contingut.getContent();
////
////            buferlector = new BufferedReader(new InputStreamReader(is));
////            linea = null;
////            while ((linea = buferlector.readLine())!=null){
////                sb.append(linea);
////            }
////            resultat = sb.toString();
////
////            if (!resultat.equals("null")){
//////                JSONObject jsonObject = new JSONObject(resultat);
//////                token = jsonObject.getString(context.getString(R.string.requestToken));
////
//////                JSONArray arrayJson = new JSONArray(resultat);
//////                int lengjason=arrayJson.length();
//////                for(int i=0;i<arrayJson.length();i++){
//////                    JSONObject objecteJson = arrayJson.getJSONObject(i);
//////                    String token = objecteJson.getString(context.getString(R.string.requestToken));
//////                    boolean success = objecteJson.getBoolean(context.getString(R.string.success));
//////                    String tokenExpires = objecteJson.getString(context.getString(R.string.expires_at));
//////                    Log.d("token", token);
//////                    Log.d("success", String.valueOf(success));
//////                    Log.d("expires at", tokenExpires);
//////                }
////            }
//        } catch (UnsupportedEncodingException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        } catch (ClientProtocolException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }


    }
    public List<String> getmMoviesNames(){
        return mMoviesNames;
    }

    public ArrayAdapter<String> get1AutoCompletAdapter(){
        return adapterAutoComplete;
    }


}
