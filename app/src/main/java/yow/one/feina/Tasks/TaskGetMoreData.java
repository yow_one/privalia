package yow.one.feina.Tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import yow.one.feina.ActivityMoviesList;
import yow.one.feina.Adapters.AdapterMovies;
import yow.one.feina.Objects.OMovies;
import yow.one.feina.R;

/**
 * Created by Yow_one on 24/01/2018.
 */

public class TaskGetMoreData extends AsyncTask<Void, Void, Void> {
    InputStream is;
    Context context;
    List<OMovies> mMovies;
    List<String> moviesId;
    AdapterMovies adapter;
    int page, count;
    String token;

    ActivityMoviesList activity;

    public void setVariables(Context context, AdapterMovies adapter, int page, List<OMovies> mMovies, List<String> moviesId, ActivityMoviesList activity){
        this.context = context;
        this.adapter = adapter;
        this.page = page;
        this.count = page;
        this.mMovies = mMovies;
        this.moviesId = moviesId;
        this.activity = activity;

    }
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        String resultat;
        HttpClient client = new DefaultHttpClient();
        List<NameValuePair> noms = new ArrayList<NameValuePair>();
        BufferedReader buferlector;
        noms.add(new BasicNameValuePair("authorization", "Bearer " + token ));
        noms.add(new BasicNameValuePair("content-type", "application/json;charset=utf-8"));
//        HttpPost enviarpost = new HttpPost("https://api.themoviedb.org/4/auth/request_token");
        HttpGet enviarGet = new HttpGet("https://api.themoviedb.org/4/list/" + moviesId.get(count) + "?api_key=" + context.getString(R.string.ApiKey));
        StringBuilder sb = new StringBuilder();

//            enviarpost.setEntity(new UrlEncodedFormEntity(noms));
        HttpResponse response = null;
        try {
            response = client.execute(enviarGet);
            HttpEntity contingut = response.getEntity();
            is=contingut.getContent();

            buferlector = new BufferedReader(new InputStreamReader(is));
            String linea = null;
            while ((linea = buferlector.readLine())!=null){
                sb.append(linea);
            }
            resultat = sb.toString();


            JSONObject jsonObjectIds = new JSONObject(resultat);
//            JSONObject movies = jsonObjectIds.getJSONObject("results");
            JSONArray array = jsonObjectIds.getJSONArray("results");
//            String[] pass1 = jsonObjectIds.toString().split(",");
            List<String> movieNames = activity.getmMoviesNames();
            for (int i = 0; i < array.length(); i++) {
                OMovies movie = new OMovies();
                JSONObject jsonMovie = (JSONObject) array.get(i);
                movieNames.add(jsonMovie.getString("title"));
                movie.setTitle(jsonMovie.getString("title"));
                movie.setOverview(jsonMovie.getString("overview"));
                movie.setYear(jsonMovie.getString("release_date"));
                movie.setUrlImage(jsonMovie.getString("poster_path"));
                mMovies.add(movie);
            }
            Log.d("movies.toString()", mMovies.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        activity.addLast();
        adapter.setLoaded();
        adapter.notifyDataSetChanged();
        activity.get1AutoCompletAdapter().notifyDataSetChanged();
    }
}
