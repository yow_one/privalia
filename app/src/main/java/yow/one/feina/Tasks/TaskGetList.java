package yow.one.feina.Tasks;

import android.content.ContentValues;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import yow.one.feina.ActivityMoviesList;
import yow.one.feina.Adapters.AdapterMovies;
import yow.one.feina.Objects.OMovies;
import yow.one.feina.R;

/**
 * Created by Yow_one on 24/01/2018.
 */

public class TaskGetList extends AsyncTask<Void, Void, Void> {
    InputStream is;
    Context context;
    int page;
    List<OMovies> mMovies;
    AdapterMovies adapterMovies;
    List<String> moviesId;
    ActivityMoviesList activity;

    public void setVariables(Context context, int page, List<OMovies> mMovies, AdapterMovies adapter, List<String> moviesId, ActivityMoviesList activity){
        this.context = context;
        this.page = page;
        this.mMovies = mMovies;
        this.adapterMovies = adapter;
        this.moviesId = moviesId;
        this.activity = activity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        String resultat;
        HttpClient client = new DefaultHttpClient();
        List<NameValuePair> noms = new ArrayList<NameValuePair>();
        noms.add(new BasicNameValuePair("authorization", "Bearer <"+ context.getString(R.string.ApiKey)+">"));
        noms.add(new BasicNameValuePair("content-type", "application/json;charset=utf-8"));
        ContentValues values=new ContentValues();
//        HttpPost enviarpost = new HttpPost("https://api.themoviedb.org/4/auth/request_token");
        HttpGet enviarGet = new HttpGet("https://api.themoviedb.org/4/list/" + page + "?api_key=" + context.getString(R.string.ApiKey));
        StringBuilder sb = new StringBuilder();
        int arrayJasona =0;
        try {
//            enviarpost.setEntity(new UrlEncodedFormEntity(noms));
            HttpResponse response = client.execute(enviarGet);
            HttpEntity contingut = response.getEntity();
            is = contingut.getContent();

            BufferedReader buferlector = new BufferedReader(new InputStreamReader(is));
            String linea = null;
            while ((linea = buferlector.readLine()) != null) {
                sb.append(linea);
            }
            resultat = sb.toString();

            JSONObject jsonObjectIds = new JSONObject(resultat);
            JSONObject movies = jsonObjectIds.getJSONObject("object_ids");
            String[] pass1 = movies.toString().split(",");
            for (int i = 0; i < pass1.length; i++) {
                String[] pass2 = pass1[i].split(":");
                moviesId.add(pass2[1].replace("\"", ""));
                Log.d("idList", pass2[1].replace("\"", ""));
            }
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        TaskGetMoreData taskaGetMoreData = new TaskGetMoreData();
        taskaGetMoreData.setVariables(context, adapterMovies, 0, mMovies, moviesId, activity);
        taskaGetMoreData.execute();
    }
}
