package yow.one.feina.Tasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.IOException;
import java.net.URL;

/**
 * Created by Yow_one on 27/01/2018.
 */

public class TaskDownloadImage extends AsyncTask {
    ImageView imageview;
    String urlName;
    Bitmap bmp;
    public void setVariables(ImageView imageView, String urlName){
        this.imageview = imageView;
        this.urlName = urlName;
    }
    @Override
    protected Object doInBackground(Object[] objects) {
        String urlImage = "https://image.tmdb.org/t/p/original/";
        try {
            URL imageURL = new URL(urlImage + urlName);
            bmp = BitmapFactory.decodeStream(imageURL.openStream());
        } catch (IOException e) {
            // TODO: handle exception
            Log.e("error", "Downloading Image Failed: " + urlImage + urlName);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        if (bmp != null) {
            imageview.setImageBitmap(bmp);
        }
    }
}
