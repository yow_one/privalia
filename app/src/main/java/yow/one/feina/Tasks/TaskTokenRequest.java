package yow.one.feina.Tasks;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import yow.one.feina.ActivityMoviesList;
import yow.one.feina.R;


/**
 * Created by Yow_one on 18/01/2018.
 */

public class TaskTokenRequest extends AsyncTask<Void, Void, Void> {
    InputStream is;
    Context context;
    String url;
    String token;
//    List<NameValuePair> listNameValuePair;
//    public void setVariables(List<NameValuePair> listNameValuePair, String url){
//        this.listNameValuePair = listNameValuePair;
//        this.url = url;
//    }
//    public void setVariables(String url){
//        this.listNameValuePair = listNameValuePair;
//        this.url = url;
//    }
    public void setVariables(Context context){
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        String resultat;


        token ="";
        boolean success;
        String tokenExpires;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL("https://api.themoviedb.org/3/authentication/token/new?api_key="+ context.getString(R.string.ApiKey));
            urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            BufferedReader buferlector = new BufferedReader(new InputStreamReader(in));
            StringBuilder sb = new StringBuilder();
            String linea = null;
            while ((linea = buferlector.readLine())!=null){
                sb.append(linea);
            }
            resultat = sb.toString();
            if (!resultat.equals("null")){
                JSONObject jsonObject = new JSONObject(resultat);
                token = jsonObject.getString(context.getString(R.string.requestToken));
                success = jsonObject.getBoolean(context.getString(R.string.success));
                tokenExpires = jsonObject.getString(context.getString(R.string.expires_at));
                SharedPreferences sharedPref = context.getSharedPreferences(
                        context.getString(R.string.requestToken), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(context.getString(R.string.requestToken), token);
                editor.putString(context.getString(R.string.expires_at), tokenExpires);
                editor.commit();
            }
        } catch (MalformedURLException e1) {
            e1.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            urlConnection.disconnect();
        }

        OkHttpClient client2 = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/octet-stream");
        RequestBody body = RequestBody.create(mediaType, "{}");
        Request request = new Request.Builder()
                .url("https://api.themoviedb.org/3/authentication/token/new?api_key="+ context.getString(R.string.ApiKey))
                .get()
                .build();

        try {
            Response response2 = client2.newCall(request).execute();
            Log.d("response" ,response2.body().string());
            final SharedPreferences sharedPref = context.getSharedPreferences(
                    context.getString(R.string.requestToken), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(context.getString(R.string.requestToken), token);
            editor.commit();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //        OkHttpClient client = new OkHttpClient();
//
//        MediaType mediaType = MediaType.parse("application/json");
//        RequestBody body = RequestBody.create(mediaType, "{\"redirect_to\":\"http://www.themoviedb.org/\"}");
//        Request request = new Request.Builder()
//                .url("https://api.themoviedb.org/4/auth/request_token")
//                .post(body)
//                .addHeader("authorization", "Bearer <"+ context.getString(R.string.app_name)+">")
//                .addHeader("content-type", "application/json;charset=utf-8")
//                .build();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if (!token.equals("")){
            Intent i = new Intent(context, ActivityMoviesList.class);
            context.startActivity(i);
        }
    }
}
