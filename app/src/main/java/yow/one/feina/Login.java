package yow.one.feina;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import yow.one.feina.Tasks.TaskTokenRequest;


public class Login extends AppCompatActivity {
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
//        sharedPreferences = getSharedPreferences("CurrentUser", MODE_PRIVATE);
//        ini();
//        if (!isLogin()){
//
//        }
        sendRequest();
    }

    public void sendRequest() {
// Create a new HttpClient and Post Header
        String url = "https://api.themoviedb.org/4/list/1?api_key=" + getString(R.string.ApiKey);
//        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
//        nameValuePairs.add(new BasicNameValuePair("Authorization", getString(R.string.ApiKey)));
//        nameValuePairs.add(new BasicNameValuePair("Content-Type", "application/json;charset=utf-8"));

        TaskTokenRequest taskTokenRequest = new TaskTokenRequest();
//        taskTokenRequest.setVariables(nameValuePairs, url);
        taskTokenRequest.setVariables(this);
        taskTokenRequest.execute();
    }

    public void ini(){}

    public boolean isLogin(){
        if (sharedPreferences.contains("user")) {
            return true;
        }
        return false;
    }
}
