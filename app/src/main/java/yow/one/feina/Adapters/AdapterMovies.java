package yow.one.feina.Adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import yow.one.feina.Interface.ILoadMore;
import yow.one.feina.Objects.OMovies;
import yow.one.feina.R;
import yow.one.feina.Tasks.TaskDownloadImage;

/**
 * Created by Yow_one on 09/12/2017.
 */


class LoadingViewHolder extends RecyclerView.ViewHolder {
    public ProgressBar progressBar;
    public LoadingViewHolder(View itemView) {
        super(itemView);
        progressBar = itemView.findViewById(R.id.progressBar);
    }
}
class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    ImageView imageMovie;
    TextView tvTitle, tvOverview, tvYear;
    LinearLayout layout;
    RecyclerView recyclerView;

    public ItemViewHolder(View itemView) {
        super(itemView);
        this.tvTitle = itemView.findViewById(R.id.txtTitle);
        this.tvOverview = itemView.findViewById(R.id.txtOverview);
        this.tvYear = itemView.findViewById(R.id.txtYear);
        this.imageMovie = itemView.findViewById(R.id.imageMovie);
    }

    @Override
    public void onClick(View v) {
        int pos = getAdapterPosition();
        Toast.makeText(v.getContext(), "You clicked " + pos, Toast.LENGTH_SHORT).show();
    }
}

public class AdapterMovies extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<OMovies> listMovies;
    final private Context context;
    private final int VIEW_TYPE_LOAADING = 1, VIEW_TYPE_ITEM = 0;
    boolean isLoading;
    int visibleTreshold=20;
    int lastVisibleItem, totalItemCount;
    private ILoadMore loadMore;
    private int position;
    RecyclerView recyclerView;
    public AdapterMovies(Context context, List<OMovies> llista, RecyclerView recyclerView){
        this.listMovies = llista;
        this.context = context;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        this.recyclerView = recyclerView;
        this.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                totalItemCount = linearLayoutManager.getItemCount();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleTreshold)) {
                    if (loadMore != null)
                        loadMore.onLoadMore();
                    isLoading = true;
                }
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return listMovies.get(position) ==null ? VIEW_TYPE_LOAADING:VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM){
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_cards, parent, false);
            return new ItemViewHolder(v);
//        }else if (viewType == VIEW_TYPE_LOAADING){
        }else {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder vh, int position) {
        if (vh instanceof ItemViewHolder){
            OMovies movie = listMovies.get(position);
            ItemViewHolder holder = (ItemViewHolder) vh;
            holder.tvTitle.setText(listMovies.get(position).getTitle());
            holder.tvOverview.setText(listMovies.get(position).getOverview());
            holder.tvYear.setText(listMovies.get(position).getYear());
            TaskDownloadImage taskDownloadImage = new TaskDownloadImage();
            taskDownloadImage.setVariables(holder.imageMovie, listMovies.get(position).getUrlImage());
            taskDownloadImage.execute();
        }else if(vh instanceof LoadingViewHolder){
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) vh;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return listMovies.size();
    }

    public void setLoaded(){
        isLoading = false;
    }

    public void setLoadMore(ILoadMore loadMore){
        this.loadMore = loadMore;
    }
//    private List<OComanda> listComanda;
//    private static ClickListener clickListener;
//
//    public void setOnItemClickListener(ClickListener clickListener) {
//        AdapterComandes.clickListener = clickListener;
//    }
//
//    public interface ClickListener {
//        void onItemClick(int position, View v);
//    }
//
//    @Override
//    public AdapterComandes.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View v = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.card_comanda, parent, false);
//        AdapterComandes.ViewHolder vh = new ViewHolder(v);
//        return vh;
//    }
//
//    @Override
//    public void onBindViewHolder(AdapterComandes.ViewHolder holder, int position) {
//
//    }
//
//    @Override
//    public int getItemCount() {
//        return 0;
//    }
}
